//
//  Tweet.m
//  
//
//  Created by Mani Kishore Chitrala on 10/23/15.
//
//

#import "Tweet.h"

@implementation Tweet

- (void)updateFromTweet:(TWTRTweet *)tweet andImageURLString:(NSString *)urlString {
    self.tweetId        = tweet.tweetID;
    self.text           = tweet.text;
    self.imageURLString = urlString;
    self.createdAt      = tweet.createdAt;
}

@end
