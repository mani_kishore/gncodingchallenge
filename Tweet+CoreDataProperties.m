//
//  Tweet+CoreDataProperties.m
//  
//
//  Created by Mani Kishore Chitrala on 10/23/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tweet+CoreDataProperties.h"

@implementation Tweet (CoreDataProperties)

@dynamic tweetId;
@dynamic text;
@dynamic imageURLString;
@dynamic createdAt;

@end
