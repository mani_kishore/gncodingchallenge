//
//  AppDelegate.h
//  GNCodingChallenge
//
//  Created by Mani Kishore Chitrala on 10/22/15.
//  Copyright © 2015 Mani Kishore Chitrala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end

