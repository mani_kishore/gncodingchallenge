//
//  TweetCell.m
//  GNCodingChallenge
//
//  Created by Mani Kishore Chitrala on 10/23/15.
//  Copyright © 2015 Mani Kishore Chitrala. All rights reserved.
//

#import "TweetCell.h"
#import <UIImageView+AFNetworking.h>

@interface TweetCell() 
@property (weak, nonatomic) IBOutlet UILabel *tweetTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flickrImageView;

@end

@implementation TweetCell

- (void)setupCell:(Tweet *)tweet {
    [self.flickrImageView setImageWithURL:[NSURL URLWithString:tweet.imageURLString] placeholderImage:[UIImage imageNamed:@"Placeholder.png"]];
    [self.tweetTextLabel setText:tweet.text];
}

@end
