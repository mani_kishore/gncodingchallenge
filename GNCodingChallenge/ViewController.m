//
//  ViewController.m
//  GNCodingChallenge
//
//  Created by Mani Kishore Chitrala on 10/22/15.
//  Copyright © 2015 Mani Kishore Chitrala. All rights reserved.
//

#import <TwitterKit/TwitterKit.h>
#import <AFNetworking.h>

#import "Constants.h"
#import "CoreDataManager.h"
#import "Tweet.h"
#import "TweetCell.h"
#import "ViewController.h"


@interface ViewController ()<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchTweetsWithFlickrImages];
}

- (void)fetchTweetsWithFlickrImages {
    
    //Logging into Twitter
    [[Twitter sharedInstance]logInWithExistingAuthToken:kTwitterAuthToken authTokenSecret:kTwitterAuthTokenSecret completion:^(TWTRSession * _Nullable session, NSError * _Nullable error) {
        if (session) {
            TWTRAPIClient *client  = [[TWTRAPIClient alloc]initWithUserID:session.userID];
            NSDictionary *params = @{@"screen_name":@"Gracenotetweets",
                                     @"count":@"20"};
            NSError *error;
            NSURLRequest *request = [client URLRequestWithMethod:@"GET" URL:kTwitterUserTimelineURLString parameters:params error:&error];
            if (request) {
                //Fetching Tweets
                [client sendTwitterRequest:request completion:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
                    if (data) {
                        NSError *jsonError;
                        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                        NSArray *tweets = [TWTRTweet tweetsWithJSONArray:jsonArray];
                        [self getImagesForTweets:tweets];
                    }
                }];
            }
        }
    }];
}

- (void) getImagesForTweets:(NSArray *)tweets {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:7];
    [params setValue:@"740fca82f76a04320306561962f438c6" forKey:@"api_key"];
    [params setValue:@"1" forKey:@"per_page"];
    [params setValue:@"url_s" forKey:@"extras"];
    [params setValue:@"json" forKey:@"format"];
    [params setValue:@"1" forKey:@"nojsoncallback"];
    [params setValue:@"flickr.photos.search" forKey:@"method"];
    
    dispatch_group_t group = dispatch_group_create();
    
    for (TWTRTweet *twtrTweet in tweets) {
        NSString *tag = [[twtrTweet.text componentsSeparatedByString:@" "]objectAtIndex:0];
        [params setValue:tag forKey:@"tags"];
        
        dispatch_group_enter(group);
        
        //Fetching flickr images
        [[AFHTTPSessionManager manager]GET:kFlickrURL parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            [[[CoreDataManager sharedManager]privateManagedObjectContext]performBlock:^{
                NSDictionary *photo = [[[responseObject objectForKey:@"photos"]objectForKey:@"photo"]firstObject];
                
                Tweet *tweet = (Tweet *)[NSEntityDescription insertNewObjectForEntityForName:@"Tweet" inManagedObjectContext:[[CoreDataManager sharedManager]privateManagedObjectContext]];
                [tweet updateFromTweet:twtrTweet andImageURLString:[photo objectForKey:@"url_s"]];
                
                dispatch_group_leave(group);
                
            }];
            
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            
            dispatch_group_leave(group);
            
        }];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [[CoreDataManager sharedManager].privateManagedObjectContext performBlock:^{
            NSError *error;
            if([[CoreDataManager sharedManager].privateManagedObjectContext hasChanges] && [[CoreDataManager sharedManager].privateManagedObjectContext save:&error]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *error;
                    [[self fetchedResultsController]performFetch:&error];
                    [self.tableView reloadData];
                });
            }
        }];
        
    });
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController == nil) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Tweet"];
        fetchRequest.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO],nil];
        fetchRequest.fetchLimit = 100;
        
        NSFetchedResultsController *controller =
        [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                            managedObjectContext:[CoreDataManager sharedManager].managedObjectContext
                                              sectionNameKeyPath:nil
                                                       cacheName:nil];
        
        controller.delegate = self;
        _fetchedResultsController = controller;
        NSError *error = nil;
        if (![self.fetchedResultsController performFetch:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        [self.tableView reloadData];
    }
    return _fetchedResultsController;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.fetchedResultsController.sections count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRows = 0;
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TweetCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TweetCell class])];
    Tweet *tweet = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (tweet) {
        [cell setupCell:tweet];
    }
    return cell;
}

#pragma mark - NSFetchedResultsControllerDelegate

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
}

@end
