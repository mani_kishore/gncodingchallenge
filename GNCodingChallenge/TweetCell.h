//
//  TweetCell.h
//  GNCodingChallenge
//
//  Created by Mani Kishore Chitrala on 10/23/15.
//  Copyright © 2015 Mani Kishore Chitrala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tweet.h"

@interface TweetCell : UITableViewCell

- (void)setupCell:(Tweet *)tweet;

@end
