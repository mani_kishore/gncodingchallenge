//
//  Tweet+CoreDataProperties.h
//  
//
//  Created by Mani Kishore Chitrala on 10/23/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tweet.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tweet (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *tweetId;
@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSString *imageURLString;
@property (nonatomic) NSDate *createdAt;

@end

NS_ASSUME_NONNULL_END
