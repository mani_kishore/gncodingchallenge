//
//  Tweet.h
//  
//
//  Created by Mani Kishore Chitrala on 10/23/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <TwitterKit/TwitterKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Tweet : NSManagedObject

- (void)updateFromTweet:(TWTRTweet *)tweet andImageURLString:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END

#import "Tweet+CoreDataProperties.h"
