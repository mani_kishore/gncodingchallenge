
App fetches latest 20 tweets of the '@Gracenotetweets' timeline and an image from Flickr for each tweet whenever it is loaded. A maximum of 100 tweets are displayed to the user in a table view. 

Image is fetched by matching the first word of the tweet as tag in Flickr api. A placeholder image is shown if Flickr doesn't return any image for the given tag. 

Fabric is used to fetch tweets.

AFNetworking library is used to communicate with Flickr api and to lazily load images to UIImageViews of the table view cells. 

Core Data with sqlite persistent store is used as a cache. Two managed object contexts are used.

A main context with concurrency type NSMainQueueConcurrencyType is tied to an NSFetchedResultsController to display tweets in a tableview.

A private context with concurrency type NSPrivateQueueConcurrencyType is used to create and save managed objects on a background thread without interrupting main queue.



Known Issues:

- No cache eviction policy. Core Data Cache is never erased. 
- Network failures and interruptions issues are not thoroughly dealt.
- As the first word of the tweet can be anything such as 'RT', a mention, etc., relevance of image with the content of the tweet can be minimal.
- Test cases aren't written.
